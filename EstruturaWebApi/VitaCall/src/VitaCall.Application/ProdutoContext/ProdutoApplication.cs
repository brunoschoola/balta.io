﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.AntiCorruption.Facades;
using VitaCall.Domain.Entities;
using VitaCall.Domain.Interfaces.Application;
using VitaCall.Domain.Interfaces.Repository;

namespace VitaCall.Application.ProdutoContext
{
    public class ProdutoApplication : IProdutoApplication
    {
       
        public async Task<IEnumerable<Produto>> ListarProdutos()
        {
            var produtos = ProdutoFacade.ListarProdutos();

            return produtos;
        }
    }
}
