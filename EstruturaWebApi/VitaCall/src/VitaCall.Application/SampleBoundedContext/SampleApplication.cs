﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.AntiCorruption.Facades;
using VitaCall.Domain.Entities;
using VitaCall.Domain.Interfaces.Application;
using VitaCall.Domain.Interfaces.Repository;

namespace VitaCall.Application.SampleBoundedContext
{
    public class SampleApplication : ISampleApplication
    {
        private readonly ISampleRepository _repo;

        public SampleApplication(ISampleRepository repo)
        {
            _repo = repo;
        }
        public async Task<string> GetSampleStringFromDatabase()
        {
            var sampleString = await _repo.GetSampleStringFromDatabase();
            Sample sample = new Sample(sampleString);
            return DoSomeStuff(sample.SampleString);
        }

        private static string DoSomeStuff(string sampleString)
        {
            return "This is a string example: " + sampleString;
        }

        public async Task<Sample> GetEstabelecimentoEndereco(string codigo)
        {
            var sample = new Sample("Endereço");
            sample.EnderecoEstabelecimento = EstabelecimentoFacade.ObterEndereco(codigo);
            return sample;
        }
    }
}
