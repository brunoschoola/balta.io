﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.AntiCorruption.Facades;
using VitaCall.Domain.Entities;
using VitaCall.Domain.Interfaces.Application;
using VitaCall.Domain.Interfaces.Repository;

namespace VitaCall.Application.UsuarioContext
{
    public class UsuarioApplication : IUsuarioApplication
    {
        private readonly IUsuarioRepository _repo;

        public UsuarioApplication(IUsuarioRepository repo)
        {
            _repo = repo;
        }

        public async Task<Usuario> GetUsuarioAD(string login)
        {
            var usuario = UsuarioFacade.ObterUsuarioAD(login);

            return usuario;
        }
    }
}
