﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VitaCall.Domain.Entities;
using VitaCall.Domain.Interfaces.Base;
using VitaCall.Domain.Interfaces.Repository;
using VitaCall.Repository.Base;

namespace VitaCall.Repository
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        private IUnitOfWork _transaction;

        public UsuarioRepository(IUnitOfWork unitOfWork, ICacheContext cacheContext) : base(unitOfWork, cacheContext)
        {
            _transaction = unitOfWork;
        }


    }
}
