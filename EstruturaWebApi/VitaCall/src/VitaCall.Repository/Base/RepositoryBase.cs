﻿using VitaCall.Domain.Interfaces.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Dapper;
using Dommel;
using System.Threading.Tasks;
using System.Data;

namespace VitaCall.Repository.Base
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        private ContextManager ContextManager { get; }
        private DbContext Context => ContextManager.Context;
        private IUnitOfWork UnitOfWork { get; }

        protected RepositoryBase(IUnitOfWork unitOfWork, ICacheContext cacheContext)
        {
            UnitOfWork = unitOfWork;
            ContextManager = new ContextManager(cacheContext);
        }

        public virtual void Add(TEntity entity)
        {
            var key = DommelMapper.Resolvers.KeyProperty(typeof(TEntity));
            var id = Context.Connection.Insert(entity, UnitOfWork.Transaction);
            key.SetValue(entity, id);
        }

        public virtual bool Update(TEntity entity)
        {
            return Context.Connection.Update(entity, UnitOfWork.Transaction);
        }

        [Obsolete("Esse metódo faz a exclusão 'física' do dado na tabela")]
        public virtual bool Delete(long id)
        {
            var obj = Context.Connection.Get<TEntity>(id);
            return Context.Connection.Delete(obj, UnitOfWork.Transaction);
        }

        public virtual TEntity GetById(object id)
        {
            return Context.Connection.Get<TEntity>(id, UnitOfWork.Transaction);
        }

        public virtual IEnumerable<TEntity> GetAll(string sql, object parameters)
        {
            return Context.Connection.Query<TEntity>(sql, parameters, UnitOfWork.Transaction);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return Context.Connection.GetAll<TEntity>(UnitOfWork.Transaction);
        }

        public IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Connection.Select(predicate, UnitOfWork.Transaction);
        }


        public virtual async Task<TEntity> GetByIdAsync(object id)
        {
            return await Context.Connection.GetAsync<TEntity>(id, UnitOfWork.Transaction);
        }

        public IEnumerable<TEntity> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public void AddAsync(TEntity item)
        {
            throw new NotImplementedException();
        }

        public bool UpdateAsync(TEntity item)
        {
            throw new NotImplementedException();
        }

        public bool DeleteAsync(long id)
        {
            throw new NotImplementedException();
        }

        public int Execute(string sql, object parameters)
        {
            return Context.Connection.Execute(sql, parameters, UnitOfWork.Transaction);
        }

        public async Task<int> ExecuteAsync(string sql, object parameters)
        {
            return await Context.Connection.ExecuteAsync(sql, parameters, UnitOfWork.Transaction);
        }

        public async Task<int> ExecuteAsync(string sql, object parameters, CommandType type)
        {
            return await Context.Connection.ExecuteAsync(sql, parameters, UnitOfWork.Transaction, 800000000, type);
        }

        public IEnumerable<TEntity> Query(string sql, object parameters)
        {
            return Context.Connection.Query<TEntity>(sql, parameters, UnitOfWork.Transaction);
        }

        public T ExecuteScalar<T>(string sql, object parameters)
        {
            return Context.Connection.ExecuteScalar<T>(sql, parameters, UnitOfWork.Transaction);
        }

        public async Task<object> ExecuteScalarAsync(string sql, object parameters)
        {
            return await Context.Connection.ExecuteScalarAsync(sql, parameters, UnitOfWork.Transaction);
        }

        public IEnumerable<T> Query<T>(string sql, object parameters, CommandType commandType = CommandType.Text)
        {
            return Context.Connection.Query<T>(sql, parameters, UnitOfWork.Transaction, commandType: commandType);
        }

        public IEnumerable<T> GetList<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return Context.Connection.Select(predicate, UnitOfWork.Transaction);
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string sql, object parameters,
            CommandType commandType = CommandType.Text) where T : class
        {
            return
                await
                    Context.Connection.QueryAsync<T>(sql, parameters, UnitOfWork.Transaction, commandType: commandType);
        }

        public async Task<IEnumerable<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await Context.Connection.SelectAsync(predicate, UnitOfWork.Transaction);
        }

        public SqlMapper.GridReader QueryMultiple(string sql, object parameters,
            CommandType commandType = CommandType.Text)
        {
            return
                Context.Connection.QueryMultiple(sql, parameters, UnitOfWork.Transaction, commandType: commandType);
        }

        public async Task<SqlMapper.GridReader> QueryMultipleAsync(string sql, object parameters, CommandType commandType = CommandType.Text)
        {
            return
                await
                    Context.Connection.QueryMultipleAsync(sql, parameters, UnitOfWork.Transaction, commandType: commandType);
        }
    }
}
