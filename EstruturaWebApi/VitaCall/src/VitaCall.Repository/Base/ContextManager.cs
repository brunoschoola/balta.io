﻿using System.Data;
using VitaCall.Domain.Interfaces.Base;

namespace VitaCall.Repository.Base
{
    public class ContextManager
    {
        public ContextManager(ICacheContext cacheContext)
        {
            _cacheContext = cacheContext;
        }

        private const string ContextKey = "ContextManager.Context";
        private readonly ICacheContext _cacheContext;


        public DbContext Context => GetContext();

        private DbContext GetContext()
        {
            var context = _cacheContext.GetOrAdd<DbContext>(ContextKey);

            if (context.Connection != null && context.Connection.State != ConnectionState.Closed)
                return context;

            _cacheContext.Remove(ContextKey);
            _cacheContext.Add(new DbContext(), ContextKey);

            return _cacheContext.GetOrAdd<DbContext>(ContextKey);
        }

        public void CloseConnection()
        {
            if (Context.Connection == null) return;
            if (Context.Connection.State == ConnectionState.Open) Context.Connection.Close();

            Context.Connection.Dispose();
            _cacheContext.Remove(ContextKey);
        }
    }
}
