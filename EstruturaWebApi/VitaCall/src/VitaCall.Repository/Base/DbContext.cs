﻿using System;
using System.Data;
using System.Diagnostics;

namespace VitaCall.Repository.Base
{
    public class DbContext : IDisposable
    {
        public DbContext()
        {
            Connection = ConnectionFactory.Create();
            Connection.Open();
        }

        public DbContext(IDbConnection connection)
        {
            Connection = connection;
        }

        public IDbConnection Connection { get; }

        private bool _disposedValue; // To detect redundant calls

        #region IDisposable Support

        private void Dispose(bool disposing)
        {
            if (_disposedValue) return;

            if (disposing)
            {
                if (Connection != null)
                {
                    Connection.Close();
                    Connection.Dispose();
                }
            }

            _disposedValue = true;
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
            Debug.Write("clean context");
        }
        #endregion
    }
}