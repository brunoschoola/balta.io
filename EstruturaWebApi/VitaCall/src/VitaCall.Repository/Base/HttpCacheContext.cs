﻿using System.Web;
using VitaCall.Domain.Interfaces.Base;

namespace VitaCall.Repository.Base
{
    public class HttpCacheContext : ICacheContext
    {
        public void Add<T>(T obj, string name) where T : class, new()
        {
            HttpContext.Current.Items[name] = obj;
        }

        public T GetOrAdd<T>(string name) where T : class, new()
        {
            if (HttpContext.Current.Items[name] == null)
            {
                Add(new T(), name);
            }

            return (T)HttpContext.Current.Items[name];
        }

        public T Get<T>(string name) where T : class, new()
        {
            return (T)HttpContext.Current.Items[name];
        }

        public void Remove(string name)
        {
            HttpContext.Current.Items.Remove(name);
        }
    }
}
