﻿using System;
using System.Data;
using VitaCall.Domain.Interfaces.Base;

namespace VitaCall.Repository.Base
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ICacheContext _cacheContext;

        public UnitOfWork(ICacheContext cacheContext)
        {
            _contextManager = new ContextManager(cacheContext);
            _cacheContext = cacheContext;
        }

        #region Properties

        private const string Key = "UnitOfWork.Transaction";
        private readonly ContextManager _contextManager;

        /// <summary>
        /// Transaction Per Request
        /// </summary>
        public IDbTransaction Transaction
        {
            get { return (IDbTransaction)_cacheContext.Get<object>(Key); }
            set { _cacheContext.Add<object>(value, Key); }
        }

        #endregion

        public void SaveChanges(bool closeConnection = false)
        {
            if (Transaction != null)
            {
                Transaction.Commit();
                Transaction = null;
            }

            if (closeConnection) CloseConnection();
        }

        public void BeginTransaction()
        {
            Transaction = _contextManager.Context.Connection.BeginTransaction();
        }

        public void Rollback(bool closeConnection = false)
        {
            if (Transaction != null)
            {
                Transaction.Rollback();
                Transaction = null;
            }
            if (closeConnection) CloseConnection();
        }

        #region IDisposable Support

        private bool _disposedValue; // To detect redundant calls

        private void Dispose(bool disposing)
        {
            if (_disposedValue) return;

            if (disposing)
            {
                if (Transaction != null)
                {
                    Transaction.Rollback();
                    Transaction = null;
                }

                _contextManager.CloseConnection();
            }

            _disposedValue = true;
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void CloseConnection()
        {
            _contextManager.CloseConnection();
        }

        #endregion
    }
}
