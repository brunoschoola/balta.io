﻿using System;
using System.Configuration;
using System.Data.Common;

namespace VitaCall.Repository.Base
{
    public static class ConnectionFactory
    {
        public static DbConnection Create()
        {
            var conStr = ConfigurationManager.ConnectionStrings["VitaCallDb"];

            if (conStr == null) throw new ConfigurationErrorsException("FailureGetConnectionString");

            //Cria uma instância de acordo dom o provider (ex: SQL Server, Oracle, MySQL)
            var provider = DbProviderFactories.GetFactory(conStr.ProviderName);

            var connection = provider.CreateConnection();

            if (connection == null) throw new ConfigurationErrorsException("FactoryConnectionIsNull");

            connection.ConnectionString = conStr.ConnectionString;

            return connection;
        }
    }
}
