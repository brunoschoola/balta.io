﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.Domain.Entities;
using VitaCall.Domain.Interfaces.Base;
using VitaCall.Domain.Interfaces.Repository;
using VitaCall.Repository.Base;

namespace VitaCall.Repository
{
    public class SampleRepository : RepositoryBase<Sample>, ISampleRepository
    {
        private IUnitOfWork _transaction;

        public SampleRepository(IUnitOfWork unitOfWork, ICacheContext cacheContext) : base(unitOfWork, cacheContext)
        {
            _transaction = unitOfWork;
        }

        public async Task<string> GetSampleStringFromDatabase()
        {
            try
            {
                _transaction.BeginTransaction();

                var Id = 1500004;
                const string sql = @"SELECT TOP 1 usuario, *
                                    FROM Pedido where Id = @Id";
                var result = await base.QueryAsync<string>(sql, new { Id });

                _transaction.SaveChanges();

                return result.FirstOrDefault();
            }
            catch (Exception)
            {
                _transaction.Rollback();
                throw;
            }
        }
    }
}
