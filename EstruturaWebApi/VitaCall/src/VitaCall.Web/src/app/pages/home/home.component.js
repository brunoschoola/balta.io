import './home.style.scss';

import HomeView from './home.view.html';
import HomeController from './home.controller';

const HomeComponent = {
  template: HomeView,
  controller: HomeController,
  controllerAs: 'vm',
};

export default HomeComponent;
