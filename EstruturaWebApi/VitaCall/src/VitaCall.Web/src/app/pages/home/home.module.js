import HomeRouting from './home.routing';
import HomeComponent from './home.component';

const MODULE_NAME = 'Home';

angular.module(MODULE_NAME, [])
  .config(HomeRouting)
  .component('home', HomeComponent);

export default MODULE_NAME;
