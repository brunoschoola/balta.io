const HomeRouting = ($stateProvider) => {
  const states = [
    {
      name: 'home',
      url: '/',
      component: 'home',
      title: 'Home',
    },
  ];

  _.forEach(states, (state) => {
    $stateProvider.state(state);
  });
};

HomeRouting.$inject = ['$stateProvider'];

export default HomeRouting;
