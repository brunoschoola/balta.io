import SacRouting from './sac.routing';
import SacComponent from './sac.component';

const MODULE_NAME = 'Sac';

angular.module(MODULE_NAME, [])
  .config(SacRouting)
  .component('sac', SacComponent);

export default MODULE_NAME;
