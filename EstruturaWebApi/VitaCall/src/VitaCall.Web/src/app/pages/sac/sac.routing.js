const SacRouting = ($stateProvider) => {
  const states = [
    {
      name: 'sac',
      url: '/sac',
      component: 'sac',
      title: 'SAC',
    },
  ];

  _.forEach(states, (state) => {
    $stateProvider.state(state);
  });
};

SacRouting.$inject = ['$stateProvider'];

export default SacRouting;
