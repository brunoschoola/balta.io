import './sac.style.scss';

import SacView from './sac.view.html';
import SacController from './sac.controller';

const SacComponent = {
  template: SacView,
  controller: SacController,
  controllerAs: 'vm',
};

export default SacComponent;
