import './example.style.scss';

import ExampleView from './example.view.html';
import ExampleController from './example.controller';

const ExampleComponent = {
  template: ExampleView,
  controller: ExampleController,
  controllerAs: 'vm',
};

export default ExampleComponent;
