const HomeRouting = ($stateProvider) => {
  const states = [
    {
      name: 'example',
      url: '/example',
      component: 'example',
      title: 'Example',
    },
  ];

  _.forEach(states, (state) => {
    $stateProvider.state(state);
  });
};

HomeRouting.$inject = ['$stateProvider'];

export default HomeRouting;
