import ExampleRouting from './example.routing';
import ExampleComponent from './example.component';

const MODULE_NAME = 'Example';

angular.module(MODULE_NAME, [])
  .config(ExampleRouting)
  .component('example', ExampleComponent);

export default MODULE_NAME;
