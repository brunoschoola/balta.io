const MODULE_NAME = 'CoreComponents';

angular.module(MODULE_NAME, []);

export default MODULE_NAME;
