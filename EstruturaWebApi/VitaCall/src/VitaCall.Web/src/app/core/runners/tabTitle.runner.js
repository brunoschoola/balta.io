const TabTitleRunner = ($transitions, tabTitleFactory) => {
  $transitions.onBefore({
    to: (state) => { return state.title; }
  }, (state) => {
    const stateTitle = state.to().title;

    if (stateTitle) { tabTitleFactory.setTitle(stateTitle); }
    if (!stateTitle) { tabTitleFactory.setDefaultTitle(); }

    return true;
  });
};

TabTitleRunner.$inject = [
  '$transitions',
  'tabTitleFactory',
];

export default TabTitleRunner;
