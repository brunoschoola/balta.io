import TabTitleRunner from './tabTitle.runner';

const MODULE_NAME = 'CoreRunners';

angular.module(MODULE_NAME, [])
  .run(TabTitleRunner);

export default MODULE_NAME;
