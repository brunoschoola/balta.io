class TabTitleFactory {
  constructor($rootScope, TAB_TITLE) {
    this.$rootScope = $rootScope;
    this.TAB_TITLE = TAB_TITLE;
  }

  setTitle(stateTitle) {
    this.$rootScope.tabTitle = `${stateTitle} | ${this.TAB_TITLE}`;
  }

  setDefaultTitle() { this.$rootScope.tabTitle = this.TAB_TITLE; }
}

TabTitleFactory.$inject = [
  '$rootScope',
  'TAB_TITLE',
];

export default TabTitleFactory;
