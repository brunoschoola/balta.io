import ResponsiveFactory from './responsive.factory';
import TabTitleFactory from './tabTitle.factory';

const MODULE_NAME = 'CoreFactories';

angular.module(MODULE_NAME, [])
  .service('responsiveFactory', ResponsiveFactory)
  .service('tabTitleFactory', TabTitleFactory);

export default MODULE_NAME;
