const CoreRouting = ($locationProvider, $urlRouterProvider) => {
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/');

  $urlRouterProvider.rule(($injector, $location) => {
    const path = $location.path();
    const hasTrailingSlash = path[path.length - 1] === '/';
    if (hasTrailingSlash) { path.substr(0, path.length - 1); }
  });
};

CoreRouting.$inject = [
  '$locationProvider',
  '$urlRouterProvider',
];

export default CoreRouting;
