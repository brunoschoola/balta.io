// DEPENDENCIES
import { name as ngStorage } from 'ngstorage';
import uiRouter from 'angular-ui-router';
import ngMaterial from 'angular-material';
import '@iamadamjowett/angular-click-outside/clickoutside.directive';

// CORE FILES
import CoreComponents from './components';
import CoreConfigs from './configs';
import CoreConstants, { APP_NAME } from './core.constants';
import CoreFactories from './factories';
import CoreResolvers from './resolvers';
import CoreRunners from './runners';
import CoreServices from './services';
import CoreRouting from './core.routing';
import CoreComponent from './core.component';

// PAGES
import Home from '../pages/home/home.module';
import Sac from '../pages/sac/sac.module';

angular.module(APP_NAME, [
  // DEPENDENCIES
  uiRouter,
  ngStorage,
  ngMaterial,
  'angular-click-outside',

  // CORE FILES
  CoreComponents,
  CoreConfigs,
  CoreConstants,
  CoreFactories,
  CoreResolvers,
  CoreRunners,
  CoreServices,

  // PAGES
  Home,
  Sac,
])
  .config(CoreRouting)
  .component('app', CoreComponent);

angular.bootstrap(document, [APP_NAME]);

export default APP_NAME;
