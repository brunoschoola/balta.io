const APP_NAME = 'vitacall`';
const TAB_TITLE = 'VitaCall';

const MODULE_NAME = 'CoreConstants';

angular.module(MODULE_NAME, [])
  .constant('APP_NAME', APP_NAME)
  .constant('TAB_TITLE', TAB_TITLE);

export { MODULE_NAME as default, APP_NAME };
