import './core.style.scss';

import CoreView from './core.view.html';
import CoreController from './core.controller';

const CoreComponent = {
  template: CoreView,
  controller: CoreController,
  controllerAs: 'core',
};

export default CoreComponent;
