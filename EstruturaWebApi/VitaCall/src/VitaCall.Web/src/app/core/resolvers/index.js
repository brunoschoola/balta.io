const MODULE_NAME = 'CoreResolvers';

angular.module(MODULE_NAME, []);

export default MODULE_NAME;
