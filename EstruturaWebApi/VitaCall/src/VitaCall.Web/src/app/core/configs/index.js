const MODULE_NAME = 'CoreConfigs';

angular.module(MODULE_NAME, []);

export default MODULE_NAME;
