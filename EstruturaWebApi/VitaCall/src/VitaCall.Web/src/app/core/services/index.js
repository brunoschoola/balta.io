const MODULE_NAME = 'CoreServices';

angular.module(MODULE_NAME, []);

export default MODULE_NAME;
