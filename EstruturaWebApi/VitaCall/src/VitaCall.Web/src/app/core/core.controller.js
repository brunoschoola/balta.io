class CoreController {
  constructor($mdSidenav, $timeout) {
    this.$mdSidenav = $mdSidenav;
    this.$timeout = $timeout;
  }

  $onInit() { this.menuIsOpen = false; }

  toggleMenu() {
    this.$mdSidenav('menu').toggle()
      .then(() => {
        this.$timeout(() => {
          this.menuIsOpen = this.$mdSidenav('menu').isOpen();
        }, 250);
      });
  }

  closeMenu() {
    if (!this.menuIsOpen) { return; }
    this.$mdSidenav('menu').close()
      .then(() => { this.menuIsOpen = false; })
  }
}

CoreController.$inject = [
  '$mdSidenav',
  '$timeout',
];

export default CoreController;
