# VitaCall Front-End

## Ambiente
- Node.js 8.12+
- npm 6.4+

## Tecnologias
- **Angular.js 1.7.4**: Framework MVVM para criação de aplicações web 
(módulos, componentes, states, etc).

- **Angular Material**: Biblioteca de componentes p/ Angular desenvolvido em cima do Material Design, o guia de design da Google.

- **Webpack 2**: Tecnologia para bundular/buildar e servir a aplicação.

- **Babel + ECMAScript 6**: Babel é um transpilador de JS para ES6, o novo core do JS.

## Comandos

`npm start` - Builda e starta o projeto c/ livereload (em memória).

`npm run build` - Builda o projeto para produção na pasta `/build`.

## Estrutura do Projeto

```
vitacall-front/
 ├── node_modules/       / Dependências
 ├── src/                / Código-fonte
 |   ├── app/            / Aplicação em si (módulos, páginas, componentes, etc)
 |   |   ├── core/       / Módulo principal onde todos os outros módulos são importados.
 |   |   ├── pages/      / Módulos das páginas
 |   └── public/         / Pasta servida pelo Webpack - contém o index.html e imagens
 ├── .babelrc            / Configurações do transpilador Babel
 ├── .eslintignore       / Ignore do ESLint
 ├── .eslintrc.json      / Configurações do ESLint
 ├── package.json        / Controle de dependências do projeto
 └── webpack.config.js   / Configurações do Webpack

```

Todos os módulos da aplicação (incluido o core) seguem a seguinte estrutura:

```
home/
 ├── home.component.js *  / Componente da página (importa a controller, style e view)
 ├── home.controller.js   / Controller da página
 ├── home.module.js *     / Módulo da página (registra o componente e expõe o módulo)
 ├── home.routing.js *    / Routing e states da página (Define os URLs)
 ├── home.factory.js      / Factory da página para salvar e manipular dados em memória
 ├── home.service.js      / Service da página para realizar HTTP requests
 ├── home.style.scss      / Estilo da página
 └── home.view.html *     / HTML da página

 * = arquivo obrigatório
```

## Responsabilidades dos recursos do Angular.js

### **Controller**
A controller deve ser a parte mais "burra" de todo módulo ou componente. Ela deve apenas chamar serviços (services, factories, etc), expor para uso na view/classe e passar o dado para o próximo passo do fluxo.

### **Componente**
O componente é um encapsulamento de **controller**, **view** e **styles**. Assim, todas as regras de comportamento e estilo ficam juntas, podendo ser reutilizadas na plataforma. Toda instancia de componente tem um escopo isolado e próprio.

### **Factory**
Uma factory serve para guardar e manipular dados salvos em memória. Um exemplo disso seria uma `UserFactory` com o objetivo de guardar os dados do usuário logado. Ela também é útil para criação de classes de formatação de dados que serão utilizados em diversos lugares da plataforma.

### **Service**
A service tem apenas uma responsabilidade: Fazer chamadas HTTP. Em alguns casos, como em uma `UserService`, todas as vezes que o usuário fizer login será necessário guardar os seus dados em uma `UserFactory`. Sendo assim, podemos chamar a `UserFactory` diretamente no callback de login do `UserService`. De qualquer forma, a regra de negócio está na factory e nunca na service.

### **Resolver**
Um resolver tem a responsabilidade de validar algum dado antes mesmo do usuário entrar
na página. Por exemplo: o módulo `dashboard` só pode ser acessado por usuários logados, assim criamos uma `UserResolver` que acessa a `UserFactory` verificando se ele está logado ou não. Se sim, o state é liberado pra acesso, caso contrário ele é redirecionado.

### **Runner**
O runner serve como uma função a ser executada no início do lifecycle de algo. Por exemplo: no `TabTitleRunner` para cada vez que a página mudar, acessar os dados de routing dela, pegar o campo de título da página e aplicar na aba do navegador.

## Nomenclaturas

Todos os arquivos de recursos do Angular (controller, service, etc) seguem a seguinte nomenclatura:

`módulo.recurso.js`

Ex:

`home.controller.js`

`home.factory.js`

...

## Style Guides

O projeto é equipado com configurações de linting seguindo os padrões de estilo da Airbnb. Para saber mais acesse os documentos abaixo:

JavaScript: [https://github.com/airbnb/javascript](https://github.com/airbnb/javascript)

CSS: [https://github.com/airbnb/css](https://github.com/airbnb/css)
