﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.Domain.Entities;
using VitaCall.Domain.Interfaces.Base;

namespace VitaCall.Domain.Interfaces.Repository
{
    public interface ISampleRepository : IRepositoryBase<Sample>
    {
        Task<string> GetSampleStringFromDatabase();
    }
}
