﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace VitaCall.Domain.Interfaces.Base
{
    public interface IRepositoryBase<TEntity> : IBase<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Query(string sql, object parameters);
        int Execute(string sql, object parameters);
        T ExecuteScalar<T>(string sql, object parameters);
        Task<object> ExecuteScalarAsync(string sql, object parameters);
        IEnumerable<T> Query<T>(string sql, object parameters, CommandType commandType);
        IEnumerable<T> GetList<T>(Expression<Func<T, bool>> predicate) where T : class;
        Task<IEnumerable<T>> QueryAsync<T>(string sql, object parameters, CommandType commandType) where T : class;
        Task<int> ExecuteAsync(string sql, object parameters);
    }
}
