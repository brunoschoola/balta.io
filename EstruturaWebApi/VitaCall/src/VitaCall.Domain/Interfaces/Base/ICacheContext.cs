﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VitaCall.Domain.Interfaces.Base
{
    public interface ICacheContext
    {
        void Add<T>(T obj, string name) where T : class, new();
        T GetOrAdd<T>(string name) where T : class, new();
        T Get<T>(string name) where T : class, new();
        void Remove(string name);
    }
}
