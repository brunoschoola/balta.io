﻿using System;
using System.Data;

namespace VitaCall.Domain.Interfaces.Base
{
    public interface IUnitOfWork : IDisposable
    {

        #region Properties

        IDbTransaction Transaction { get; set; }

        #endregion

        #region Methods

        void SaveChanges(bool closeConnection = false);
        void BeginTransaction();
        void Rollback(bool closeConnection = false);
        void CloseConnection();

        #endregion
    }
}
