﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace VitaCall.Domain.Interfaces.Base
{
    public interface IBase<TEntity> where TEntity : class
    {
        TEntity GetById(object id);
        Task<TEntity> GetByIdAsync(object id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAllAsync();
        void Add(TEntity entity);
        void AddAsync(TEntity item);
        bool Update(TEntity entity);
        bool UpdateAsync(TEntity item);
        bool Delete(long id);
        bool DeleteAsync(long id);
        IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> predicate);
    }
}
