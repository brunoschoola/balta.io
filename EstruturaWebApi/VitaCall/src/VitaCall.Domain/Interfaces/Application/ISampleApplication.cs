﻿using System.Threading.Tasks;
using VitaCall.Domain.Entities;

namespace VitaCall.Domain.Interfaces.Application
{
    public interface ISampleApplication
    {
        Task<string> GetSampleStringFromDatabase();
        Task<Sample> GetEstabelecimentoEndereco(string codigo);
    }
}
