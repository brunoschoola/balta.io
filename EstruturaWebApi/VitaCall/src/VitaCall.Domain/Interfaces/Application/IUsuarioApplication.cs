﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.Domain.Entities;

namespace VitaCall.Domain.Interfaces.Application
{
    public interface IUsuarioApplication
    {
        Task<Usuario> GetUsuarioAD(string login);
    }
}
