﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.Common;

namespace VitaCall.Domain.Entities
{
    public class Sample
    {
        public Sample(string sampleString)
        {
            this.SampleString = sampleString;
        }

        string sampleString;

        public string SampleString
        {
            get { return sampleString; }
            private set
            {
                int limit = 20;
                AssertionConcern.AssertArgumentLength(value, limit, "String superior a " + limit.ToString() + " caracteres");
                this.sampleString = value;
            }
        }

        public string EnderecoEstabelecimento { get; set; }
    }
}
