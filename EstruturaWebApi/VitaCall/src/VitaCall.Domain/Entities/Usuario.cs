﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.Common;

namespace VitaCall.Domain.Entities
{
    public class Usuario
    {
        public Usuario()
        {            
        }

        public string Login { get; set; }

        public string Nome { get; set; }

        public string Endereco { get; set; }

        public string Cep { get; set; }

        public string Gestor { get; set; }

        public string Telefone { get; set; }

        public string Departamento { get; set; }

        public string DisplayName { get; set; }

        public string Cargo { get; set; }

        string email;

        public string Email
        {
            get { return email; }
            set
            {
                AssertionConcern.AssertArgumentEmail(value);
                this.email = value;
            }
        }
                
        string emailGestor;

        public string EmailGestor
        {
            get { return emailGestor; }
            set
            {
                AssertionConcern.AssertArgumentEmail(value);
                this.emailGestor = value;
            }
        }
    }
}
