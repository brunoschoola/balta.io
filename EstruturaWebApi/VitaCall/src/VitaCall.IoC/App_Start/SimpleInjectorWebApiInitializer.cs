﻿using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using System.Web.Http;

namespace VitaCall.IoC.App_Start
{
    public static class SimpleInjectorWebApiInitializer
    {
        public static Container Container { get; set; }

        public static void Initialize(HttpConfiguration config)
        {
            Container = new Container();
            Container.Options.DefaultScopedLifestyle = new SimpleInjector.Lifestyles.AsyncScopedLifestyle();
            InitializeContainer(Container);
            Container.RegisterWebApiControllers(config);
            Container.Verify();
            config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(Container);
        }

        private static void InitializeContainer(Container container)
        {
            IoCContainer.Register(container, Lifestyle.Scoped);
        }

    }
}
