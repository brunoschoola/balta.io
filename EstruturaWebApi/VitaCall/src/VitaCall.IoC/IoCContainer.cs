﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Text;
using VitaCall.Application.ProdutoContext;
using VitaCall.Application.SampleBoundedContext;
using VitaCall.Application.UsuarioContext;
using VitaCall.Domain.Interfaces.Application;
using VitaCall.Domain.Interfaces.Base;
using VitaCall.Domain.Interfaces.Repository;
using VitaCall.Repository;
using VitaCall.Repository.Base;

namespace VitaCall.IoC
{
    public static class IoCContainer
    {
        public static void Register(Container container, Lifestyle lifestyle)
        {
            container.Register<ContextManager>(lifestyle);

            //Apps
            container.Register<ISampleApplication, SampleApplication>(lifestyle);
            container.Register<IUsuarioApplication, UsuarioApplication>(lifestyle);
            container.Register<IProdutoApplication, ProdutoApplication>(lifestyle);


            //Base
            container.Register<ICacheContext, HttpCacheContext>(lifestyle);
            container.Register<IUnitOfWork, UnitOfWork>(lifestyle);

            //Repos
            container.Register<ISampleRepository, SampleRepository>(lifestyle);
            container.Register<IUsuarioRepository, UsuarioRepository>(lifestyle);
        }
    }
}
