﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using VitaCall.Common.CustomExceptions;
using VitaCall.Domain.Interfaces.Application;

namespace VitaCall.API.Controllers
{
    public class ProdutoController : ApiController
    {
        private readonly IProdutoApplication _app;

        public ProdutoController(IProdutoApplication app)
        {
            _app = app;
        }

        public async Task<HttpResponseMessage> GetProducts()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, await _app.ListarProdutos());
            }
            catch (TotvsBridgeException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
