﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using VitaCall.Common.CustomExceptions;
using VitaCall.Domain.Interfaces.Application;

namespace VitaCall.API.Controllers
{
    public class ValuesController : ApiController
    {
        private readonly ISampleApplication _app;

        public ValuesController(ISampleApplication app)
        {
            _app = app;
        }

        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, await _app.GetSampleStringFromDatabase());
            }
            catch (InvalidOperationException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        public async Task<HttpResponseMessage> GetFromTOTVS(string codigo)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, await _app.GetEstabelecimentoEndereco(codigo));
            }
            catch(TotvsBridgeException ex)
            {
                return Request.CreateResponse(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}
