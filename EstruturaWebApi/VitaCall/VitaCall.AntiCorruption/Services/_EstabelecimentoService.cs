﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.AntiCorruption.Dtos;
using VitaCall.AntiCorruption.TotvsBridge;
using VitaCall.Common.CustomExceptions;

namespace VitaCall.AntiCorruption.Services
{
    public static class EstabelecimentoService
    {
        public static EstabelecimentoEnderecoDto ObterEnderecoEstabelecimento(string codigo)
        {
            try
            {
                using (var ws = new TotvsBridgeObjClient())
                {
                    var jsonIn = String.Format(@"{{""ttEstabelecimento"": [{{""CodigoEstabelecimento"": ""{0}""}}]}}", codigo);
                    var jsonOut = "";
                    ws.TotvsBridge("Business\\All\\getEstabelecimento.p", "delta", jsonIn, out jsonOut);

                    return JsonConvert.DeserializeObject<EstabelecimentoEnderecoDto>(jsonOut);
                }

            }
            catch (TotvsBridgeException)
            {
                throw;
            }
        }
    }
}
