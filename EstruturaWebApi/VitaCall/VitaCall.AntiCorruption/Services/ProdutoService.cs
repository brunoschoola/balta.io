﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.AntiCorruption.Dtos;
using VitaCall.AntiCorruption.TotvsBridge;
using VitaCall.Common.CustomExceptions;

namespace VitaCall.AntiCorruption.Services
{
    public class ProdutoService
    {
        public static ProdutosDTO ListarProdutos()
        {
            try
            {
                string codigo = "";
                using (var ws = new TotvsBridgeObjClient())
                {
                    var jsonIn = String.Format(@"", codigo);
                    var jsonOut = "";
                    ws.TotvsBridge("Business\\All\\xxxxx.p", "delta", jsonIn, out jsonOut);

                    return JsonConvert.DeserializeObject<ProdutosDTO>(jsonOut);
                }

            }
            catch (TotvsBridgeException)
            {
                throw;
            }
        }
    }
}
