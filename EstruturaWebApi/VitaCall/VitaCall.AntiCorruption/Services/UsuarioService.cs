﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using VitaCall.AntiCorruption.Dtos;

namespace VitaCall.AntiCorruption.Services
{
    public static class UsuarioService
    {
        static HttpClient client = new HttpClient();

        public async static Task<ADRootObjectDTO> BuscarUsuarioAD(string login)
        {
            ADRootObjectDTO rootAD = null;
            HttpResponseMessage response = await client.GetAsync("https://sa-br-daf.sa.corp.airliquide.com/webdafapi/api/ActiveDirectory/Usuario/" + login);

            if (response.IsSuccessStatusCode)
            {
                rootAD = await response.Content.ReadAsAsync<ADRootObjectDTO>();
            }
            return rootAD;
        }
        
    }
}
