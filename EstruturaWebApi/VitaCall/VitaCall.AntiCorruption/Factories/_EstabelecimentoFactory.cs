﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VitaCall.AntiCorruption.Factories
{
    public static class EstabelecimentoFactory
    {
        public static string ObterEnderecoEstabelecimentoTOTVS(string codigo)
        {
            var estab = Services.EstabelecimentoService.ObterEnderecoEstabelecimento(codigo);
            return estab.ttResult.First().Endereco;
        }

    }
}
