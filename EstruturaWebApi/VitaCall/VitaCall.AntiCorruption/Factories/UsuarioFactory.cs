﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.Domain.Entities;

namespace VitaCall.AntiCorruption.Factories
{
    public class UsuarioFactory
    {
        public static Usuario ObterUsuarioAD(string login)
        {
            var usuarioRootAD = Services.UsuarioService.BuscarUsuarioAD(login);

            Usuario usuario = null;

            if (usuarioRootAD != null && usuarioRootAD.Result != null && usuarioRootAD.Result.Properties != null)
            {
                var properties = usuarioRootAD.Result.Properties;

                usuario.Nome = properties.name.FirstOrDefault();
                usuario.Login = login;
                usuario.Endereco = properties.streetaddress.FirstOrDefault();
                usuario.Cargo = properties.title.FirstOrDefault();
                usuario.Departamento = properties.department.FirstOrDefault();
                usuario.Cep = properties.postalcode.FirstOrDefault();
                usuario.DisplayName = properties.displayname.FirstOrDefault();
                usuario.Email = properties.mail.FirstOrDefault();
                usuario.EmailGestor = properties.extensionattribute11.FirstOrDefault();
                usuario.Gestor = properties.manager.FirstOrDefault();
                usuario.Telefone = properties.telephonenumber.FirstOrDefault();
            }

            return usuario;
            
        }
    }
}
