﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.AntiCorruption.Factories;
using VitaCall.Domain.Entities;

namespace VitaCall.AntiCorruption.Facades
{
    public class UsuarioFacade
    {
        public static Usuario ObterUsuarioAD(string login)
        {
            return UsuarioFactory.ObterUsuarioAD(login);
        }
    }
}
