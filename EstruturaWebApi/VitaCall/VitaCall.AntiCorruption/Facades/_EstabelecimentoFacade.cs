﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VitaCall.AntiCorruption.Factories;

namespace VitaCall.AntiCorruption.Facades
{
    public static class EstabelecimentoFacade
    {
        public static string ObterEndereco(string codigo)
        {
            return EstabelecimentoFactory.ObterEnderecoEstabelecimentoTOTVS(codigo);
        }
    }
}
