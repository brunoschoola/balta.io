﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VitaCall.AntiCorruption.Dtos
{
    public class EstabelecimentoEnderecoDto
    {
        public List<TtResult> ttResult { get; set; }
    }

    public class TtResult
    {
        public string CodigoEstabelecimento { get; set; }
        public string NomeEstabelecimento { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
    }
}
