﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VitaCall.AntiCorruption.Dtos
{
    public class ADRootObjectDTO
    {
        public string Path { get; set; }
        public Properties Properties { get; set; }
    }

    public class Properties
    {
        public string[] samaccountname { get; set; }
        public string[] streetaddress { get; set; }
        public string[] company { get; set; }
        public string[] postalcode { get; set; }
        public string[] adspath { get; set; }
        public string[] autoreplymessage { get; set; }
        public string[] extensionattribute14 { get; set; }
        public string[] givenname { get; set; }
        public string[] extensionattribute8 { get; set; }
        public string[] manager { get; set; }
        public string[] employeeid { get; set; }
        public string[] extensionattribute2 { get; set; }
        public string[] description { get; set; }
        public string[] userprincipalname { get; set; }
        public string[] telephonenumber { get; set; }
        public string[] name { get; set; }
        public string[] distinguishedname { get; set; }
        public string[] department { get; set; }
        public string[] displayname { get; set; }
        public string[] mail { get; set; }
        public string[] title { get; set; }
        public string[] extensionattribute11 { get; set; }
        public string[] sn { get; set; }
    }
}
